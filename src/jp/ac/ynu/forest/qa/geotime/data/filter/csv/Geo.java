package jp.ac.ynu.forest.qa.geotime.data.filter.csv;

import java.util.Arrays;

/**
 * CSV上のGeoレコード
 */
public class Geo {
    private String continent;
    private String area;
    private String smallArea;
    private String country;
    private String region;

    public Geo(String continent, String area, String smallArea, String country, String region) {
        this.continent = continent;
        this.area = area;
        this.smallArea = smallArea;
        this.country = country;
        this.region = region;
    }

    public Geo() {
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSmallArea() {
        return smallArea;
    }

    public void setSmallArea(String smallArea) {
        this.smallArea = smallArea;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return "Geo{" +
                "continent='" + continent + '\'' +
                ", area='" + area + '\'' +
                ", smallArea='" + smallArea + '\'' +
                ", country='" + country + '\'' +
                ", region='" + region + '\'' +
                '}' + "\n";
    }

//    public boolean contains(Geo geo){
//        if(this.equals(geo)){
//            return true;
//        }
//
//        if (continent != null ? !continent.equals(geo.continent) : geo.continent != null) return false;
//        if (area != null ? !area.equals(geo.area) : geo.area != null) return false;
//        if (smallArea != null ? !smallArea.equals(geo.smallArea) : geo.smallArea != null) return false;
//        if (country != null ? !country.equals(geo.country) : geo.country != null) return false;
//        return !(region != null ? !region.equals(geo.region) : geo.region != null);
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Geo)) return false;

        Geo geo = (Geo) o;

        if (continent != null ? !continent.equals(geo.continent) : geo.continent != null) return false;
        if (area != null ? !area.equals(geo.area) : geo.area != null) return false;
        if (smallArea != null ? !smallArea.equals(geo.smallArea) : geo.smallArea != null) return false;
        if (country != null ? !country.equals(geo.country) : geo.country != null) return false;
        return !(region != null ? !region.equals(geo.region) : geo.region != null);

    }

    @Override
    public int hashCode() {
        int result = continent != null ? continent.hashCode() : 0;
        result = 31 * result + (area != null ? area.hashCode() : 0);
        result = 31 * result + (smallArea != null ? smallArea.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (region != null ? region.hashCode() : 0);
        return result;
    }

    public boolean contains(String location) {
        return containsLabel(continent, location) ||
                containsLabel(area, location) ||
                containsLabel(smallArea , location)||
                containsLabel(country, location)||
                containsLabel(region, location);
    }

    private boolean containsLabel(String label, String key){
        String[] continents = splitSameLocations(label);
        int i = Arrays.binarySearch(continents, key);
        return (i >= 0);
    }


    private String[] splitSameLocations(String label){
        if(label != null && label.contains("@")) {
            return label.split("@");
        }else if(label != null){
            return new String[]{label};
        }else{
            return new String[0];
        }
    }
}
