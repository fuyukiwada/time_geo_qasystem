package jp.ac.ynu.forest.qa.geotime.data.centertest;

/**
 * Created by kosasa on 15/08/25.
 */
public class Question {
    private String bigQuestion;
    private String middleQuestion;
    private String smallQuestion;
    private String[] choice;

    public Question() {
    }

    public String getBigQuestion() {
        return bigQuestion;
    }

    public void setBigQuestion(String bigQuestion) {
        this.bigQuestion = bigQuestion;
    }

    public String getMiddleQuestion() {
        return middleQuestion;
    }

    public void setMiddleQuestion(String middleQuestion) {
        this.middleQuestion = middleQuestion;
    }

    public String getSmallQuestion() {
        return smallQuestion;
    }

    public void setSmallQuestion(String smallQuestion) {
        this.smallQuestion = smallQuestion;
    }

    public String[] getChoice() {
        return choice;
    }

    public void setChoice(String[] choice) {
        this.choice = choice;
    }
}
