package jp.ac.ynu.forest.qa.geotime.data.filter.csv;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kosasa on 15/08/11.
 */
public class GeoParser {
    public static final String path = "/Users/kosasa/dev/works/lab/time_geo_qasystem/res/geo.csv";

    public GeoParser() {
    }

    public List<Geo> listFromFile() {
        // ライブラリ使用。
        // 単純に,でsplitかけると空要素や"に対応できないため
        CSVParserBuilder parserBuilder = new CSVParserBuilder();
        CSVParser parser = parserBuilder.build();

        File file = new File(path);
        List<Geo> geoList = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] item = parser.parseLine(line);
                if (item.length > 5) {
                    Geo geo = new Geo(item[0], item[1], item[2], item[3], item[4]);
                    geoList.add(geo);
                }
            }

            if (geoList.size() > 0) {
                geoList.remove(0);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return geoList;
    }

    public static void main(String[] args) {
        GeoParser parser = new GeoParser();
        List<Geo> geoList = parser.listFromFile();
        System.out.println("geoList = " + geoList);
    }
}
