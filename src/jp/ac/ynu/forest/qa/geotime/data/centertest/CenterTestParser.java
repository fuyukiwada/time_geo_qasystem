package jp.ac.ynu.forest.qa.geotime.data.centertest;

import forest.sakamoto.exam.QuestionFormat;
import forest.sakamoto.exam.national_center_test.xml.*;
import forest.sakamoto.exam.national_center_test.xml.choice.Choice;
import forest.sakamoto.exam.national_center_test.xml.choice.Choices;
import forest.sakamoto.exam.national_center_test.xml.choice.ChoicesImpl;
import scala.Option;
import scala.collection.JavaConversions;
import scala.collection.Seq;
import scala.collection.immutable.Map;
import scala.xml.NodeSeq;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fuyuki on 2015/07/24.
 */
public class CenterTestParser {

    public List<String> parse(int year) {
        List<String> list = new ArrayList<>();
        NationalCenterTestParser parser;
        parser = new NationalCenterTestParser();
        // 結果が空になるときは、実行パスのチェック
        // Init.scala, l49
        // リソースが参照できてない可能性がある
        Option<NationalCenterTestQuestion> result = parser.parse(year);

        Seq<BigQuestion> questions = result.get().questions();
        List<BigQuestion> bigQuestions = JavaConversions.seqAsJavaList(questions);

        for (BigQuestion bigQuestion : bigQuestions) {
            Seq<MiddleQuestion> middleQuestionSeq = bigQuestion.middleQuestions();
            List<MiddleQuestion> middleQuestions = JavaConversions.seqAsJavaList(middleQuestionSeq);

            for (MiddleQuestion middleQuestion : middleQuestions) {
                Seq<SmallQuestion> smallQuestionSeq = middleQuestion.smallQuestions();
                List<SmallQuestion> smallQuestions = JavaConversions.seqAsJavaList(smallQuestionSeq);

                for (SmallQuestion smallQuestion : smallQuestions) {
                    // 対象はfactoidのみ
                    if (smallQuestion.questionFormat().equals(QuestionFormat.Factoid())) {
                        // 問題文

                        System.out.println(bigQuestion.question());
                        System.out.println(middleQuestion.context());
                        System.out.println(smallQuestion.question());

                        Option<Map<String, String>> sentences = smallQuestion.sentences();
                        Option<String> underLinePart = smallQuestion.underLinePart();
                        int id = smallQuestion.id();
                        Option<NodeSeq> explanation = smallQuestion.explanation();
                        ChoicesImpl choices = smallQuestion.choices();


                        // 空の場合あり
                        System.out.println("sentences = " + sentences);
                        // 空Noneの場合あり
                        System.out.println("underLinePart = " + underLinePart);

                        System.out.println("id = " + id);
                        // タグがついてる
                        System.out.println("explanation = " + explanation);
                        // キャスト必須
                        System.out.println("choices = " + choices);

                        if (choices instanceof Choice<?>) {
                            Choice choice = (Choice) choices;
                            // 例 choices = List(後漢, 魏, 隋, 百済)
                            System.out.println("choice = " + choice);
                        } else {
                            Choices multiple = (Choices) choices;
                            //multiple = List(二月(三月)革命, 五月(六月)革命, 七月(八月)革命, 十月(十一月)革命)
                            // multiple = List(Map(b -> ドイツ, d -> アメリカ合衆国, a -> ソ連, c -> イギリス), Map(b -> ソ連, d -> イギリス, a -> ドイツ, c -> アメリカ合衆国), Map(b -> イギリス, d -> ドイツ, a -> ソ連, c -> アメリカ合衆国), Map(b -> アメリカ合衆国, d -> ソ連, a -> イギリス, c -> ドイツ))
                            System.out.println("multiple = " + multiple);
                        }



                        System.out.println();
                        System.out.println();


                        String s = smallQuestion.question().toString();
                        list.add(s);

                        // 選択肢
//                        ChoicesImpl choices = smallQuestion.choices();


                    }
                }
            }
        }
        return list;
    }

    public static void main(String[] args) {
        CenterTestParser parser = new CenterTestParser();
        List<String> list = parser.parse(1997);

//        System.out.println(list);
    }

}
