package jp.ac.ynu.forest.qa.geotime.data.text;

import forest.sakamoto.ir.fulltext.indri.IndriKeywordQuery;
import forest.sakamoto.ir.fulltext.indri.IndriResult;
import forest.sakamoto.ir.fulltext.indri.IndriRetriever;
import scala.collection.JavaConversions;
import scala.collection.Seq;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fuyuki on 2015/07/24.
 */
public class Textbook {

    // 注意1 インデックスの場所は絶対パスである必要がある
    // 注意2 インデックスのパスは100字以下である必要がある
    // TODO Fileで相対パスで読み込み、絶対パスに置き換える
    private static final String INDEX_FOLDER = "/Users/kosasa/dev/works/lab/worldhistoryindriindex/index/";

    enum Index {
        TOKYO_SHOSEKI_A("tokyoshoseki/world_history_textbook_a_index"),
        TOKYO_SHOSEKI_B("tokyoshoseki/world_history_textbook_b_index"),
        TOKYO_SHOSEKI_SHINSEN_B("tokyoshoseki/world_history_textbook_b_shinsen_index"),
        YAMAKAWA_GLOSSARY("yamakawa/world_history_glossary_index"),
        YAMAKAWA_B("yamakawa/world_history_textbook_b_index");


        public final String mFile;

        Index(String file) {
            mFile = file;
        }
    }

    private Seq<String> getIndexFile(Index index) {
        List<String> indexfile = new ArrayList<>();
        indexfile.add(INDEX_FOLDER + index.mFile);
        return JavaConversions.asScalaBuffer(indexfile);
    }

    public List<IndriResult> search(Index index, String query) {
        IndriKeywordQuery q = new IndriKeywordQuery(query);

        IndriRetriever retriever = new IndriRetriever(getIndexFile(index));
        Seq<IndriResult> resultSeq = retriever.retrieve(q);

        return JavaConversions.seqAsJavaList(resultSeq);
    }

    public static void main(String[] args) {
        Textbook textbook = new Textbook();
        List<IndriResult> china = textbook.search(Index.TOKYO_SHOSEKI_B, "中国");
        System.out.println(china);
    }
}
