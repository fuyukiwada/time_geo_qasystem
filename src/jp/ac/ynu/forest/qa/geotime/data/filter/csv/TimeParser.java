package jp.ac.ynu.forest.qa.geotime.data.filter.csv;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kosasa on 15/08/19.
 */
public class TimeParser {
    public static final String path = "/Users/kosasa/dev/works/lab/time_geo_qasystem/res/geo.csv";

    public TimeParser() {
    }

    public List<Time> listFromFile(){
        CSVParserBuilder parserBuilder = new CSVParserBuilder();
        CSVParser parser = parserBuilder.build();

        File file = new File(path);
        List<Time> timeList = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] item = parser.parseLine(line);
                Time time = new Time();
                time.setCols(item);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return timeList;
    }

}
