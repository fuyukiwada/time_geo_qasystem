package jp.ac.ynu.forest.qa.geotime.data.filter;

import jp.ac.ynu.forest.qa.geotime.data.filter.csv.Geo;
import jp.ac.ynu.forest.qa.geotime.data.filter.csv.GeoParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kosasa on 15/08/26.
 */
public class GeoFilter {
    private static GeoFilter instance;
    private List<Geo> geoList;

    private GeoFilter(){
        GeoParser parser = new GeoParser();
        geoList = parser.listFromFile();

    }

    public static GeoFilter getInstance() {
        if(instance == null)
            instance = new GeoFilter();
        return instance;
    }

    public List<Geo> findGeo(String location){
        List<Geo> result = new ArrayList<>();

        for(Geo geo : geoList){
         // locationがgeoのどこかのカラムに引っかかったら追加
            if(geo.contains(location)){
                result.add(geo);
            }
        }

        return result;
    }

    /**
     * 問題文が持つGeoの集合に選択肢の持つGeoの集合が1つでも含まれているかどうか
     * @param source
     * @param target
     * @return
     */
    public boolean has(List<Geo> source, List<Geo> target){
        for(Geo geo : source){
            for(Geo t : target){
                if(geo.equals(t)){
                    return true;
                }
            }
        }

        return false;
    }
}
