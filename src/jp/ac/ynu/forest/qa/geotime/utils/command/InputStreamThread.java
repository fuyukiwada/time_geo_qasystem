package jp.ac.ynu.forest.qa.geotime.utils.command;

/**
 * Created by kosasa on 15/08/11.
 */

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * InputStreamを読み込むスレッド
 */
public class InputStreamThread extends Thread {

    private BufferedReader br;

    private List<String> list = new ArrayList<>();

    public InputStreamThread(InputStream is) {
        br = new BufferedReader(new InputStreamReader(is));
    }

    public InputStreamThread(InputStream is, String charset) {
        try {
            br = new BufferedReader(new InputStreamReader(is, charset));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                list.add(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 文字列取得
     */
    public List<String> getStringList() {
        return list;
    }
}