package jp.ac.ynu.forest.qa.geotime.utils.command;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by fuyuki on 2015/05/26.
 */

public class CommandExecutor {
    private static final int TOKEN_SIZE = 100;

    public static CommandResult executeSimple(String cmd, String input) throws IOException {
        if (cmd == null || cmd.length() == 0) {
            throw new IllegalArgumentException("empty command error");
        }

        // command
        List<String> command = new ArrayList<>();
        command.addAll(Arrays.asList(cmd.split(" ")));

        Process process = new ProcessBuilder(command).start();

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
        bw.write(input);
        bw.flush();
        bw.close();

        //InputStreamのスレッド開始
        InputStreamThread it = new InputStreamThread(process.getInputStream());
        InputStreamThread et = new InputStreamThread(process.getErrorStream());
        it.start();
        et.start();

        //プロセスの終了待ち
        try {
            process.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //InputStreamのスレッド終了待ち
        try {
            it.join();
            et.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        CommandResult result = new CommandResult(it.getStringList(), et.getStringList(), process.exitValue());

        process.destroy();

        return result;
    }

    public static List<String> execute(String cmd, String inputText) throws  IOException {
        if (cmd == null || cmd.length() == 0) {
            throw new IllegalArgumentException("empty command error");
        }

        // command
        List<String> command = new ArrayList<>();
        command.addAll(Arrays.asList(cmd.split(" ")));

        List<String> result = new ArrayList<>();

        String commandPath = command.size() > 0 ? command.get(0) : "";
        boolean isMecab = commandPath.toLowerCase().endsWith("mecab") || commandPath.toLowerCase().contains("mecab.exe");
        boolean isCabocha = commandPath.toLowerCase().endsWith("cabocha") || commandPath.toLowerCase().contains("cabocha.exe");

        // input text
        List<String> lineSplitData = new ArrayList<>();
        lineSplitData.addAll(Arrays.asList(inputText.split("\n")));

        List<String> inputs = mergeInputIfRequire(lineSplitData, isMecab || isCabocha);

        for (String d2 : inputs) {
            Process process = new ProcessBuilder(command).start();

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
            bw.write(d2);
            bw.flush();
            bw.close();

            //InputStreamのスレッド開始
            InputStreamThread it = new InputStreamThread(process.getInputStream());
            it.start();

            //プロセスの終了待ち
            try {
                process.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //InputStreamのスレッド終了待ち
            try {
                it.join();

                result.addAll(it.getStringList());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            process.destroy();
        }

        return result;
    }

    /**
     * 必要な場合、適度なサイズにデータをマージ
     * @param lineSplitData
     * @param isRequired
     * @return
     */
    private static List<String> mergeInputIfRequire(List<String> lineSplitData, boolean isRequired){
        List<String> inputs;

        if (lineSplitData.size() > TOKEN_SIZE && isRequired) {
            inputs = new ArrayList<>();

            int token_size = 0;
            StringBuilder tmp = new StringBuilder();
            for (String line : lineSplitData) {
                tmp.append(line).append("\n");
                if (token_size > TOKEN_SIZE) {
                    inputs.add(tmp.toString());
                    tmp = new StringBuilder();
                    token_size = 0;
                } else {
                    token_size++;
                }
            }
            inputs.add(tmp.toString());
        } else {
            inputs = lineSplitData;
        }

        return inputs;
    }


}