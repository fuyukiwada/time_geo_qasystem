package jp.ac.ynu.forest.qa.geotime.utils.mecab;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fuyuki on 2015/08/07.
 */
public class MeCabParser {
    private List<String> input;

    public MeCabParser(List<String> input) {
        this.input = input;
    }

    public List<Word> parse() {
        List<Word> words = new ArrayList<>();
        for (String targetLine : input) {
            if (targetLine == null) {
                break;
            }
            if (targetLine.equals("EOS")) {
                continue;
            }

            //品詞
            String[] tabSplit = targetLine.split("\t");
            String targetType = tabSplit[1];
            String[] info = targetType.split(",");

            Word word = new Word();
            word.setSurface(tabSplit[0]);
            word.setPartOfSpeech(info[0]);
            words.add(word);
        }
        return words;
    }

    public List<Word> getNoun() {
        List<Word> parse = parse();
        List<Word> result = new ArrayList<>();
        for (Word word : parse) {
            if (word.getPartOfSpeech().equals("名詞")) {
                result.add(word);
            }

        }
        return result;
    }

}
