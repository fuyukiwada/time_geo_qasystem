package jp.ac.ynu.forest.qa.geotime.utils.mecab;

/**
 * Created by fuyuki on 2015/08/07.
 */
public class Word {
    private String surface;
    private String partOfSpeech;

    public Word() {
    }

    @Override
    public String toString() {
        return "Word{" +
                "surface='" + surface + '\'' +
                ", partOfSpeech='" + partOfSpeech + '\'' +
                '}';
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public String getPartOfSpeech() {
        return partOfSpeech;
    }

    public void setPartOfSpeech(String partOfSpeech) {
        this.partOfSpeech = partOfSpeech;
    }
}
