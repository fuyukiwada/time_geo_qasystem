package jp.ac.ynu.forest.qa.geotime.utils;

/**
 * Created by fuyuki on 2015/05/26.
 */
import java.text.Normalizer;

public class NFKCNormalizer {
    /**
     *
     * @param charSeq
     * @return
     */
    public static String normalize(CharSequence charSeq) {
        return Normalizer.normalize(charSeq, Normalizer.Form.NFKC);
    }

    /**
     *
     * @param c
     * @return
     */
    public static char normalize(char c) {
        return  Normalizer.normalize(String.valueOf(c), Normalizer.Form.NFKC).charAt(0);
    }

    /**
     *
     * @param charSeq
     * @return
     */
    public static String normalizeWithCheck(CharSequence charSeq) {
        return isNormalized(charSeq) ? (String) charSeq : normalize(charSeq);
    }

    /**
     *
     * @param c
     * @return
     */
    public static char normalizeWithCheck(char c) {
        return isNormalized(c) ? c : normalize(c);
    }

    /**
     *
     * @param charSeq
     * @return
     */
    public static boolean isNormalized(CharSequence charSeq) {
        return Normalizer.isNormalized(charSeq, Normalizer.Form.NFKC);
    }

    /**
     *
     * @param c
     * @return
     */
    public static boolean isNormalized(char c) {
        return isNormalized(String.valueOf(c));
    }
}

