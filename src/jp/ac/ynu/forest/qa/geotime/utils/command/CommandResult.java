package jp.ac.ynu.forest.qa.geotime.utils.command;

import java.util.List;

/**
 * Created by kosasa on 15/08/11.
 */
public class CommandResult {
    private final List<String> output;
    private final List<String> error;
    private final int exitValue;

    public CommandResult(List<String> output, List<String> error, int exitValue) {
        this.output = output;
        this.error = error;
        this.exitValue = exitValue;
    }

    public List<String> getOutput() {
        return output;
    }

    public List<String> getError() {
        return error;
    }

    public int getExitValue() {
        return exitValue;
    }
}
