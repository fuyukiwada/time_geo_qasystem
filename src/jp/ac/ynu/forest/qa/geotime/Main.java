package jp.ac.ynu.forest.qa.geotime;

import jp.ac.ynu.forest.qa.geotime.utils.command.CommandExecutor;
import jp.ac.ynu.forest.qa.geotime.utils.mecab.MeCabParser;
import jp.ac.ynu.forest.qa.geotime.utils.mecab.Word;
import jp.ac.ynu.forest.qa.geotime.data.centertest.CenterTestParser;

import java.util.List;

/**
 * Created by fuyuki on 2015/06/02.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        CenterTestParser parser = new CenterTestParser();
        List<String> list = parser.parse(1997);
        for (String item : list) {
            List<String> data_mecab = CommandExecutor.execute("C:\\Program Files (x86)\\MeCab\\bin\\mecab.exe", item);
            MeCabParser meCabParser = new MeCabParser(data_mecab);
            List<Word> parse = meCabParser.getNoun();
            System.out.println("parse = " + parse);
        }

    }
}
